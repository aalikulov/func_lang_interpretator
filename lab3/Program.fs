﻿module Program
    open System
    type Expr =
     |Add of Expr*Expr
     |Sub of Expr*Expr
     |Div of Expr*Expr
     |Mul of Expr*Expr
     |Value of int

    type Lexem = Int of int | Op of char | Start | End | Let | Equ | Id of string | If | Then | Else | Reroute

    type State = WhiteSpace | IntSpace of int | StringSpace of string

    let Recognize s=match s with
        "if" -> If
        |"then" -> Then
        |"else" -> Else
        |"let" -> Let
        |otherText->Id(otherText)        

    let rec makeLexemFlow (s:string) state =
     if s="" then
         match state with
             WhiteSpace -> []
           | IntSpace(n) -> [Int(n)]
           |StringSpace(s) -> [Recognize(s)]
     else
       match s.[0] with
        ' ' -> 
          match state with
            WhiteSpace -> makeLexemFlow (s.Substring(1)) WhiteSpace
           |IntSpace(n) -> Int(n)::(makeLexemFlow (s.Substring(1)) WhiteSpace)
           |StringSpace(str) -> Recognize(str)::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | x when x>='0'&&x<='9' ->
          match state with
             WhiteSpace -> makeLexemFlow (s.Substring(1)) (IntSpace(Convert.ToInt32(x.ToString())))
            |IntSpace(n) -> makeLexemFlow (s.Substring(1)) (IntSpace(n*10+(Convert.ToInt32(x.ToString()))))
            |StringSpace(str) -> Recognize(str)::(makeLexemFlow (s.Substring(1)) (IntSpace(Convert.ToInt32(x.ToString()))))
        | '+' | '-' | '*' | '/' ->
          match state with
             WhiteSpace ->  Op(s.[0])::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |IntSpace(n) -> Int(n)::Op(s.[0])::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |StringSpace(str) -> Recognize(str)::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | '(' -> 
          match state with
             WhiteSpace -> Start::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |IntSpace(n) -> Int(n)::Start::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |StringSpace(str) -> Recognize(str)::Start::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | ')' -> 
          match state with
             WhiteSpace -> End::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |IntSpace(n) -> Int(n)::End::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |StringSpace(str) -> Recognize(str)::End::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | '=' -> 
          match state with
             WhiteSpace -> Equ::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |IntSpace(n) -> Int(n)::Equ::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |StringSpace(str) -> Recognize(str)::Equ::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | '>' -> 
          match state with
             WhiteSpace -> Reroute::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |IntSpace(n) -> Int(n)::Reroute::(makeLexemFlow (s.Substring(1)) WhiteSpace)
            |StringSpace(str) -> Recognize(str)::Reroute::(makeLexemFlow (s.Substring(1)) WhiteSpace)
        | char when (char>='a'&&char<='z')||(char>='A'&&char<='Z')||char='_' -> 
          match state with
            WhiteSpace -> makeLexemFlow (s.Substring(1)) (StringSpace(char.ToString()))
           |IntSpace(n) -> Int(n)::(makeLexemFlow (s.Substring(1)) (StringSpace(char.ToString())))
           |StringSpace(str) -> makeLexemFlow (s.Substring(1)) (StringSpace(String.Concat(str,char.ToString())))


    let parse x =
        let rec parse' = function 
              [] -> failwith "Error"
            | Op(c)::l ->
                let (t1,l1) = parse' l
                let (t2,l2) = parse' l1
                match c with
                  '+' -> (Add(t1,t2),l2)
                | '-' -> (Sub(t1,t2),l2)
                | '*' -> (Mul(t1,t2),l2)
                | '/' -> (Div(t1,t2),l2)
            | Int(n)::l -> (Value(n),l) 
        let (t,l) = parse' x
        if List.isEmpty l then t
        else failwith "Error"

    let rec LexListPrint = function
      [] -> Console.WriteLine ""
      | Op(c)::l -> Console.Write "OP ";Console.Write c; Console.Write " "; LexListPrint l
      | Int(c)::l -> Console.Write "INT ";Console.Write c;Console.Write " "; LexListPrint l
      | Start::l -> Console.Write "( "; LexListPrint l
      | End::l ->Console.Write ") "; LexListPrint l
      | Let::l ->Console.Write "let "; LexListPrint l
      | Equ::l ->Console.Write "= "; LexListPrint l
      | Id(s)::l ->Console.Write "ID "; Console.Write s;Console.Write " "; LexListPrint l
      | If::l ->Console.Write "if "; LexListPrint l
      |Else::l ->Console.Write "else ";LexListPrint l
      |Then::l ->Console.Write "then ";LexListPrint l
      |Reroute::l -> Console.Write "> "; LexListPrint l

    let write (s:string) = Console.Write s

    let rec ParsedExprPrint = function
        Add(t1,t2) -> write ""; ParsedExprPrint t1; write "+"; ParsedExprPrint t2; write ""
        |Sub(t1,t2) -> write ""; ParsedExprPrint t1; write "-("; ParsedExprPrint t2; write ""
        |Mul(t1,t2) -> write "("; ParsedExprPrint t1; write ")*("; ParsedExprPrint t2; write ")"
        |Div(t1,t2) -> write "("; ParsedExprPrint t1; write ")/("; ParsedExprPrint t2; write ")"
        |Value(n) -> write (n.ToString())

    let rec SolveExpr = function
        Add(t1,t2) -> (+) (SolveExpr t1) (SolveExpr t2)
        |Sub(t1,t2) -> (-) (SolveExpr t1) (SolveExpr t2)
        |Mul(t1,t2) -> (*) (SolveExpr t1) (SolveExpr t2)
        |Div(t1,t2) -> (/) (SolveExpr t1) (SolveExpr t2)
        |Value(n) -> n


    
    makeLexemFlow "* + 4 22 4 let d = (default 4+ 123_adsa12) s" WhiteSpace |> LexListPrint    
//    makeLexemFlow "* + 4 22 4" WhiteSpace |> parse |> SolveExpr |> Console.Write
